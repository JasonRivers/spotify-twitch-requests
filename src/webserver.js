var express = require('express');
var querystring = require('querystring');
var request = require('request');
var path = require('path');

var myBaseUrl = 'http://localhost:8888/';
var app;

twitchOptions = {
  authURL: "https://id.twitch.tv/oauth2/authorize?",
  authURL2: "https://id.twitch.tv/oauth2/token?",
  redirect_uri: myBaseUrl + "twitch-callback",
  scope: "chat:read chat:edit user_read",
}
spotifyOptions = {
  authURL: "https://accounts.spotify.com/authorize?",
  authURL2: 'https://accounts.spotify.com/api/token',
  redirect_uri: myBaseUrl + "spotify-callback",
  scope: "user-read-private user-read-email playlist-read-private playlist-modify-private user-read-playback-state user-modify-playback-state streaming"
}

var generateRandomString = function(length) {
  var text = '';
  var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

  for (var i = 0; i < length; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return text;
};

module.exports = {
  init:() => {

  twitchOptions.client_id = process.env.TWITCH_CLIENT_ID;
  twitchOptions.client_secret = process.env.TWITCH_CLIENT_SECRET;
  spotifyOptions.client_id = process.env.SPOTIFY_CLIENT_ID;
  spotifyOptions.client_secret = process.env.SPOTIFY_CLIENT_SECRET;

  console.log(twitchOptions);
  console.log(spotifyOptions);
    app = express();

    app.get('/', (req, res) => { res.sendFile(path.resolve(__dirname + '/../html/index.html'))});
    app.get('/css/style.css', (req, res) => { res.sendFile(path.resolve(__dirname + '/../html/css/style.css'))});

    app.get('/auth/:service', function(req, res) {
      type = req.params.service;
      switch (type) {
        case 'spotify':
          options = spotifyOptions;
          break;;
        case 'twitch':
          options = twitchOptions;
          break;;
      }
      redirect = options.authURL +
        querystring.stringify({
          response_type: 'code',
          client_id: options.client_id,
          scope: options.scope,
          redirect_uri: options.redirect_uri,
          state: generateRandomString(16)
        })
      res.redirect(redirect);
    })

    app.get('/twitch-callback', function (req, res) {
      var code = req.query.code;
  twitchOptions.client_id = process.env.TWITCH_CLIENT_ID;
  twitchOptions.client_secret = process.env.TWITCH_CLIENT_SECRET;
      options = twitchOptions;
      var  authOptions = {
        url: options.authURL2,
        form: {
          code,
          redirect_uri: options.redirect_uri,
          grant_type: 'authorization_code',
          client_id: options.client_id,
          client_secret: options.client_secret
        },
        json: true
      }
      module.exports.getoAuthToken('twitch', res, authOptions);
    });

    app.get('/spotify-callback', function (req, res) {
      var code = req.query.code;
  spotifyOptions.client_id = process.env.SPOTIFY_CLIENT_ID;
  spotifyOptions.client_secret = process.env.SPOTIFY_CLIENT_SECRET;
      options = spotifyOptions;
      var authOptions = {
        url: options.authURL2,
        form: {
          code,
          redirect_uri: options.redirect_uri,
          grant_type: 'authorization_code'
        },
        headers: {
          'Authorization': 'Basic ' + (new Buffer(options.client_id + ':' + options.client_secret).toString('base64'))
        },
        json: true
      }
      module.exports.getoAuthToken('spotify', res, authOptions);
    })
    app.listen(8888);
  },
  getoAuthToken: (service, res, authOptions) => {
    request.post(authOptions, function(error, response, body) {
      console.log(body);
      if (error) {
        console.log(error);
      }
      if (body && body.error) {
        console.error(body.error);
      }
      var seconds = Math.floor(new Date().getTime() / 1000);
      seconds = seconds + parseInt(body.expires_in);
      global.config[service] = {
        access_token: body.access_token,
        refresh_token: body.refresh_token,
        expires_at: seconds
      }
      global.writeConfig(service);
      eval(service + '.init()');
      res.redirect('/');
    })

  }
}