var request = require('request');

var base_url = 'https://api.spotify.com/v1/';

module.exports = {
  init:() => {
      module.exports.api({
        path: 'me',
        type: 'get'
      }, (data) => {
        global.config.spotify.userid = data.id;

        module.exports.api({
          path: 'me/playlists',
          type: 'get'
        }, (playlists) => {
          if (playlists.items) {
            for (i=0; i<playlists.items.length; i++) {
              if (playlists.items[i].name === "SongRequests") {
                global.config.spotify.playlistId = playlists.items[i].id;
              }
            }
            if (!global.config.spotify.playlistId) {
              module.exports.api({
                path: 'users/' + global.config.spotify.userid +'/playlists',
                type: 'post',
                formData: {
                  name: 'SongRequests',
                  public: false
                }
              }, (newplaylist) => {
                global.config.spotify.playlistId = newplaylist.id
                global.writeConfig('spotify');
                console.log('Using spotify playlist id ' + global.config.spotify.playlistId);
              })
            } else {
                global.writeConfig('spotify');
                console.log('Using spotify playlist id ' + global.config.spotify.playlistId);
            }
          }
        })
      })
  },
  api: async (apiRequest, c) => {
    var seconds = Math.floor(new Date().getTime() / 1000);
    if (global.config.spotify.expires_at < seconds) {
      token = await module.exports.refreshToken();
      if (!token) {
        return false;
      }
    }
    var options = {
      url: base_url + apiRequest.path,
      headers: {
        'Authorization': 'Bearer ' + global.config.spotify.access_token,
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }
    }


    switch (apiRequest.type) {
      case "post":
        options.form = JSON.stringify(apiRequest.formData);
        options.method = 'POST';
        request.post(options, ( err, res, body) => {
          if (err) {
            console.error(err);
          } else {
            c(JSON.parse(body));
          }
        })
        break;
      case "get":
        options.method = 'GET';
        request(options, ( err, res, body) => {
          if(err) {
            console.log(err);
          }
          if (body) {
            c(JSON.parse(body));
          } else {
            c(false);
          }
        })

        break;
      case "put":
        if (apiRequest.formData) {
          options.form = JSON.stringify(apiRequest.formData);
        }
        options.method = 'PUT';
        request.put(options, ( err, res, body) => {
          if (res && res.statusCode && res.statusCode === 204) {
            c(true);
          } else {
          c(JSON.parse(body));
          }
        })
        break;
      case "delete":
        if (apiRequest.formData) {
          options.form = JSON.stringify(apiRequest.formData);
        }
        options.method = 'DELETE';
        request.delete(options, ( err, res, body) => {
          c(JSON.parse(body));
        })
        break;
    }
  },
  search: (searchQuery, c) => {
    apiRequest = {
      path: 'search?q=' + searchQuery +'&type=track',
      type: 'get'
    }
    module.exports.api(apiRequest, (data) => {
//      for (i=0; i<data.tracks.items; i++) {
//        console.log(data.tracks.items[i]);
//      }
      c(data.tracks.items[0]);
    })
  },
  addTrack(uri, c) {
    module.exports.api({
      path: 'playlists/' + global.config.spotify.playlistId + '/tracks',
      type: 'post',
      formData: { uris: [uri]}
    }, (data) => {
      c(data);
    })
  },
  refreshToken: () => {
    request.post({
      url: 'https://accounts.spotify.com/api/token',
      headers: {
        'Authorization': 'Basic ' + (new Buffer(process.env.SPOTIFY_CLIENT_ID + ':' + process.env.SPOTIFY_CLIENT_SECRET).toString('base64')),
        'Accept': 'application/json'
      },
      form: {
        grant_type: 'refresh_token',
        refresh_token: global.config.spotify.refresh_token
      }
    }, (err, response, body) => {
      if (err) {
        console.log(err)
        return false;
      }
      data = JSON.parse(body);
      if (data && data.access_token) {
        var seconds = Math.floor(new Date().getTime() / 1000);
        seconds = seconds + data.expires_in;
        global.config.spotify.access_token = data.access_token;
        global.config.spotify.expires_at = seconds;
        global.writeConfig('spotify');
        return body.access_token;
      } else  {
        return false;
      }

    })
  },
  getPlayerDetails:(c) => {
    module.exports.api({
      path: 'me/player',
      type: 'get'
    }, (data) => {
      c(data);
    })
  },
  getPlaylist: (playlist, c) => {
    module.exports.api({
      path: 'playlists/' + playlist + '/tracks',
      type: 'get'
    }, (result) => {
      c(result);
    })
  },
  setPlaying: (context, c) => {
    module.exports.api({
      path: 'me/player/play',
      type: 'put',
      formData: {
        context_uri: context
      }
    }, (ret) => {
      if (ret) {
        c(ret);
      } 
    })
  },
  removeFromPlaylist: (playlist, trackuri, c) => {
    module.exports.api({
      path: 'playlists/' + playlist + '/tracks',
      type: 'delete',
      formData: {
        tracks: [
          {
            uri: trackuri
          }
        ]
      }
    }, (ret) => {
      if (ret) {
        c(ret);
      }
    })
  },
  toggleShuffle: (state, c) => {
    if (!state) {
      state=false;
    }
    console.log('Setting Shuffle State');
    module.exports.api({
      path: 'me/player/shuffle?state=' + state,
      type: 'put',
    }, (res) => {
      if (c) {
        c(res);
      }
    })
  }
}
