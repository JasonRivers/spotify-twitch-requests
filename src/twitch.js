const tmi = require('tmi.js');
const request = require('request');

var twitchBot;
var base_url = 'https://api.twitch.tv/kraken/';

requests=[];

function onChatHandler(channel, userstate, message, self) {
  if (self) return
    // Get the command
    var command = message.replace(/ .*/, '');
    var args = message.replace(/[^\s]+ /, '').split(' ');

    switch (command) {
      case "!sr":
        if (args && args[0] === command) {
          twitchBot.say(channel, 'Usage: ' + command + ' {song name}');
          break;
        }
        searchData = args.join(' ');
        spotify.search(searchData, (data) => {
          if (!data || !data.uri) {
            twitchBot.say(channel, 'You muppet, That song does\'t exist');
          } else {
            twitchBot.say(channel, 'Adding ' + data.artists[0].name + ' - ' + data.name + ' to song queue');
            requests.push({
              username: userstate.username,
              songuri: data.uri,
              songName: data.artists[0].name + ' - ' + data.name
            })
            spotify.addTrack(data.uri, (playlist) => {
              console.log('Track: ' + data.artists[0].name + ' - ' + data.name + ' Added to playlist ');
            });
          }
        })
        break;
      case "!song":
        spotify.getPlayerDetails((response) => {
          if (response && response.item && response.item.album && response.item.album.artists) {
            artist = response.item.album.artists[0].name;
          }
          song = response.item.name;
          twitchBot.say(channel, "Currently playing: " + song + " by " + artist);
        })
        break;
      case "!queue":
        spotify.getPlaylist(global.config.spotify.playlistId, (ret) => {
          if (ret && ret.items && ret.items.length > 0) {
            songs = 'Songs in queue: ';
            for (var i=0; i<ret.items.length; i++) {
              console.log(JSON.stringify(ret.items[i].track.album, null, 2));
              artist = ret.items[i].track.album.artists[0].name;
              song = ret.items[i].track.album.name;
              songs+=artist + ' - ' + song + ', ';
            }
            twitchBot.say(channel, songs);
          }

        })
    }
}

//setInterval(() => {
//  console.log(requests);
//}, 60000)

module.exports = {
  init: () => {
    apiRequest = {

    }
    module.exports.api({ path: 'user', type: 'get', }, (ret) => {
      global.config.twitch.username=ret.name;
      global.writeConfig('twitch');
      console.log(ret);
   twitchBot = new tmi.client({
      options: {
        debug: true,
        clientId: "j7qmgyvnkefvwdm2ccrae3fafgaztw"
      },
      connection: {
        reconnect: true
      },
      identity: {
        username: global.config.twitch.username,
        password: "oauth:" + global.config.twitch.access_token
      },
      channels: ["#" + global.config.twitch.username]
   }) ;
   twitchBot.connect();
   twitchBot.on('connected', () => {
     twitchBot.say('#'+ global.config.twitch.username, global.application.displayName + ' (' + global.application.version + ') is connected');
   })
//
   twitchBot.on('chat', onChatHandler);
    })
  },
  api: async (apiRequest, c) => {
    var seconds = Math.floor(new Date().getTime() / 1000);
    if (global.config.twitch.expires_at < seconds) {
      token = await oauth.refreshToken();
      if (!token) {
        return false;
      }
    }
    var options = {
      url: base_url + apiRequest.path,
      headers: {
        'Authorization': 'OAuth ' + global.config.twitch.access_token,
        'client_id': 'wumdxh0slydjwi75f2p51e3kw4mo77',
        'Content-Type': 'application/json',
        'Accept': 'application/vnd.twitchtv.v5+json'
      }
    }
    switch (apiRequest.type) {
      case "get":
        options.method = 'GET';
        request(options, ( err, res, body) => {
          if(err) {
            console.log(err);
          }
          if (body) {
            c(JSON.parse(body));
          } else {
            c(false);
          }
        })

        break;
    }
  }
}