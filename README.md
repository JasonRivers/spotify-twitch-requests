# Spotify-Twitch-Requests

## About this project

This project is a demonstration of using twitch chat to control a spotify playlist.
This will automatically create a playlist in your spotify called "SongRequests" and will play tracks in-order until there are no songs left in the request queue. 

This was created for educational purposes during stream to demonstrate the use of OAUTH, Twitch Chat and using API's, The video can be found here: https://www.twitch.tv/videos/510178932

## How it works

The bot will add songs to a SongRequest playlist, and check this playlist every 10 seconds, at the end of the currently playing song it will automatically switch to playing from the Request queue, once the queue is empty it will automatically return to the last playing playlist.

## Twitch Commands

| Command | Details | example |
|   ---   |   ---   |   ---   |
| !sr {song name }   |  Request Song | !sr Muse new born |
| !song   |  Show currently playing song | !song |

## Setting up

You will need to populate `src/.env` with the spotify client details which you can get from https://developer.spotify.com/dashboard, E.G.
```bash
SPOTIFY_CLIENT_ID=spotify-client-id
SPOTIFY_CLIENT_SECRET=spotify-client-secret
SPOTIFY_REDIRECT_URI=http://localhost:8888/spotify-callback
```

You will also need to rename `config/twitch.example.json` to `config/twitch.json` and fill in the details for authentication, you can get the oauth token from https://twitchapps.com/tmi/

## Licence

MIT

Copyright 2019 Jason Rivers <jason@jasonrivers.co.uk>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.